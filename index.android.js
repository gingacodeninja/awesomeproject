import React from 'react';
import {
	Image,
	AppRegistry,
	StyleSheet,
	View,
	Text,
	ListView,
	TouchableHighlight
} from 'react-native';

var styles = {
	root: {
		position: 'absolute',
		top: 0,
		left: 0,
		bottom: 0,
		right: 0
	},
	listViewItem: {
		flex: 1,
		backgroundColor: '#ddd',
		padding: 10,
		borderBottomWidth: 1,
		borderBottomColor: '#ccc',
		flexDirection: 'row',
		alignItems: 'center'
	},
	avatar: {
		width: 40,
		height: 40,
		resizeMode: 'contain'
	},
	username: {
		left: 10
	}
};

var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
var data = ds.cloneWithRows(['John', 'Joel', 'James', 'Jimmy', 'Jackson']);

var DefaultAvatarUrl = 'https://cdn1.iconfinder.com/data/icons/ninja-things-1/1772/ninja-simple-512.png';

function hello() {
	// Do nothing yet, required for onpress highlight to work
}

const App = () => {
  return (
  		<View>
  			<ListView dataSource={data}
  				renderRow={(rowData) => <TouchableHighlight onPress={hello} underlayColor="white">
  					<View style={styles.listViewItem}>
  						<Image style={styles.avatar} resizeMode={Image.resizeMode.contain} source={{uri: DefaultAvatarUrl}} />
 						<Text style={styles.username}>{rowData}</Text>
					</View>
				</TouchableHighlight>} />
  		</View>
	);
}

AppRegistry.registerComponent('AwesomeProject', () => App);